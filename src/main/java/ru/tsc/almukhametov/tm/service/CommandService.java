package ru.tsc.almukhametov.tm.service;

import ru.tsc.almukhametov.tm.api.ICommandRepository;
import ru.tsc.almukhametov.tm.api.ICommandService;
import ru.tsc.almukhametov.tm.model.Command;

public class CommandService implements ICommandService {

    private final ICommandRepository commandRepository;

    public CommandService(final ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Override
    public Command[] getTerminalCommands() {
        return commandRepository.getTerminalCommands();
    }

}
