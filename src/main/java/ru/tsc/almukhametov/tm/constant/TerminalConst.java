package ru.tsc.almukhametov.tm.constant;

public class TerminalConst {

    public static final String ABOUT = "about";
    public static final String HELP = "help";
    public static final String VERSION = "version";
    public static final String INFO = "info";
    public static final String COMMANDS = "commands";
    public static final String ARGUMENTS = "arg";
    public static final String EXIT = "exit";


}
