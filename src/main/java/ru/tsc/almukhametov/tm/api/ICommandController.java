package ru.tsc.almukhametov.tm.api;

public interface ICommandController {

    void showInfo();

    void showAbout();

    void showVersion();

    void showCommands();

    void showArguments();

    void showHelp();

    void showErrArg();

    void showErrCommand();

    void exit();

}
