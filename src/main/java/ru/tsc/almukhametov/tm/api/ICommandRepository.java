package ru.tsc.almukhametov.tm.api;

import ru.tsc.almukhametov.tm.model.Command;

public interface ICommandRepository {

    Command[] getTerminalCommands();

}
